# README #

This repo contains the code to test the webhook integration from formstack with python/bottle app running in the azure which is used to send the data from formstack to sendgrid to email the user.

### Credentials ###

**Formstack**: 

email - webhook@outlook.com

password - P@ssw0rd@2016

**Sendgrid**:

email - webhook@outlook.com

password - P@ssw0rd@2016

**Test email account**:

email - webhook@outlook.com

password - P@ssw0rd@2016

**Sample form** - https://webhook.formstack.com/forms/support_contact_form_copy

### How to test ###
1. Visit the link https://webhook.formstack.com/forms/support_contact_form_copy
2. Fill out the form and submit
3. open test email account and see the email with data that you just submitted

### Customization ###
* If you want to send information to multiple email accounts then go to this url https://www.formstack.com/admin/form/settings/2263316#emails-and-redirects (use login info provided above) and in the section "After the Form is Submitted" click on "Submit Action" and edit the Webhook URL query parameter "email" with multiple email address seprated by comma (Ex. email=webhook@outlook.com,xyz@abc.com).