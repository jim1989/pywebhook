"""
Routes and views for the bottle application.
"""
import json
from urllib import urlopen

from bottle import route, view, template, request, SimpleTemplate, HTTPResponse
from datetime import datetime
import sendgrid

SECRET = ['kjfkldf89ryirjeciqnr3r89rn893ncrwocimw94row4ir']

@route('/')
@route('/home')
@view('index')
def home():
    """Renders the home page."""
    return dict(
        year=datetime.now().year
    )


def process_data(data):

    return {
        "first_name": data['Name']['first'] if data.get('Name', {}).has_key('first') else '',
        "last_name": data['Name']['last'] if data.get('Name', {}).has_key('last') else '',
        "email": data['Email'] if data.get('Email') is not None else '',
        "phone": data['Phone'] if data.get('Phone') is not None else '',
        "address": data['Address']['address'] if data.get('Address', {}).has_key('address') else '',
        "address2": data['Address']['address2'] if data.get('Address', {}).has_key('address2') else '',
        "city": data['Address']['city'] if data.get('Address', {}).has_key('city') else '',
        "state": data['Address']['state'] if data.get('Address', {}).has_key('state') else '',
        "country": data['Address']['country'] if data.get('Address', {}).has_key('country') else '',
        "zip": data['Address']['zip'] if data.get('Address', {}).has_key('zip') else '',
        "comment": data['Comments'] if data.get('Comments') is not None else ''
    }


@route('/webhook', method='POST')
def webhook_receive():

    postdata_raw = request.body.read()
    postdata_json = json.loads(postdata_raw)
    postdata_secret = postdata_json.get('HandshakeKey')

    if postdata_secret in SECRET:
        output = template('views/confirmation.tpl', data=process_data(postdata_json))

        query_param = request.query.decode()
        email = query_param.get('email')

        if email is not None:

            sg = sendgrid.SendGridClient('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', raise_errors=True)

            message = sendgrid.Mail()
            message.add_to(email.split(','))
            message.set_subject('New Submission')
            message.set_html(output)
            message.set_from('Test User <webhook@gmail.com>')

            file_attachment_url = postdata_json.get('Upload a File', '')
            if file_attachment_url:
                attachment_name = file_attachment_url.split('/')[-1]
                message.add_attachment(attachment_name, urlopen(file_attachment_url))
            status, msg = sg.send(message)

            if status != 200:
                return HTTPResponse(status=400, body=json.dumps({"detail": msg}))

            return HTTPResponse(status=200)
        else:
            return HTTPResponse(status=400, body=json.dumps({"detail": "email is not provided"}))
    else:
        return HTTPResponse(status=400)